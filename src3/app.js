import React, { PureComponent, Component } from 'react'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as PageActions from "./actions";
import Page2 from "./page2";
class App extends Component {
   constructor(props){
       super(props);
       this.state = {
        input: {
            name: "",
            email: ""
        }
    }
   }
  render() {
    return (
      <div>
           <h1>{this.props.state.sayi}</h1>
           <Page2></Page2>
           
      </div>
    )
  }
}
const mapStateToProps = state => ({
    state: state
  });
  
  const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(PageActions, dispatch)
  });
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(App);