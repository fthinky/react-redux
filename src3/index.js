import React from 'react';
import ReactDOM from 'react-dom';
import { Provider} from 'react-redux';
import store  from './store'
import App  from './app'

export  class Index extends React.Component {
  render () {
    return <div>
      Merhaba
      <Provider store={store}>
     <App></App>
    </Provider>
    </div>;
  }
}
ReactDOM.render( <Index />, document.getElementById("container"));

// import React from "react";
// import { render } from "react-dom";
// import { Provider } from "react-redux";

// import store from "./store";
// import Page from "./page1";

// const App = () => (
//   <Provider store={store}>
//     <Page />
//   </Provider>
// );

// render(<App />, document.getElementById("container"));
