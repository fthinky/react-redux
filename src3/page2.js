import React, { Component } from 'react'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as PageActions from "./actions";
class Page2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: {
                name: "",
                email: ""
            }
        }
    }

    arttir = sayi => {
        this.props.actions.setSayiState(sayi);

    };


    render() {
        return (
            <div>
                Sayfa2
        <h1>{this.props.state.sayi}</h1>
                <button onClick={event => this.arttir(1)}>increment</button>
            </div>
        )
    }
}
const mapStateToProps = state => ({

    state: state

});
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(PageActions, dispatch)
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Page2);