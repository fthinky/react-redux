import * as PageActionTypes from "./actionTypes";
// import { SET_SAYI } from './actionTypes'

export function setSayiState(options) {
  return {
    type: PageActionTypes.SET_SAYI,
    options: options
  };
}
