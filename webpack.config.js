const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
// const htmlPlugin = new HtmlWebPackPlugin({
//   template: "./src/index.html",
//   filename: "./index.html"
// });

module.exports = {
    entry: './src3/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"



            }
        ]
    },
     mode: 'production',
    //  devServer: {
    //     historyApiFallback: true,
    //     hot: true,
    //     inline: true,
    //     progress: true,
    //     contentBase: './app',
    //     port: 8082,
    //   },
    
    //  plugins: [new HtmlWebpackPlugin()]
    //  plugins: [htmlPlugin]

    //  ,
     plugins: [
        new HtmlWebpackPlugin({template: './index.html'})
    ]

    // plugins:[
    //     new webpack.HotModuleReplacementPlugin()
    // ],
    // devServer:{
    //     hot: true,
    //     contentBase:'/'
    // },
    //  devTool: 'inline-source-map'
};
